package lab2.dc.unifil.br;

import java.util.List;
import java.util.Random;

public class Classificadores {

    public static void bogosort(List<Integer> lista) {
        while (!isOrdenada(lista)) {
            int i, j; do {
                i = rng.nextInt(lista.size());
                j = rng.nextInt(lista.size());
            } while (lista.get(i) < lista.get(j));

            permutar(lista, i, j);
        }
    }

    private static void permutar(List<Integer> lista, int a, int b) {
        Integer permutador = lista.get(a);
        lista.set(a, lista.get(b));
        lista.set(b, permutador);
    }

    private static boolean isOrdenada(List<Integer> lista) {
        for (int i = 1; i < lista.size(); i++)
            if (lista.get(i-1) > lista.get(i))
                return false;

        return true;
    }

    private static Random rng = new Random("Seed constante repetível".hashCode());
}
